from django.shortcuts import render,redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse
from .shop import ShopifyOperations
import requests
import json
from django.contrib.auth.decorators import login_required
def home(request):
	return render(request, 'stores/home.html')

def signup(request):
	if request.method == 'POST':
		if request.POST['password1'] == request.POST['password2']:
			try:
				User.objects.get(username=request.POST['username'])
				return render(request, 'stores/signup.html', {'error':'Username already exists'})
			except User.DoesNotExist:	
				user = User.objects.create_user(request.POST['username'], password = request.POST['password1'])
				login(request,user)
				return redirect('dashboard')
		else:
			return render(request, 'stores/signup.html', {'error' : 'Passwords do not match'})
	else:
		return render(request, 'stores/signup.html')	


def dashboard(request):
	if  request.user.is_authenticated:
		# return redirect('login')
		shopify = ShopifyOperations()
		product_list = shopify.inventory_info()
		# print (product_list)
		return render(request, "stores/dashboard.html", {'product_list':product_list})	
	return redirect('login')	

def checkout(request):
	order_items = []
	for key in request.POST:
		if key != 'csrfmiddlewaretoken':
			variant_quanity = request.POST[key].split(',')
			order_items.append(variant_quanity)
	# print (order_items)
	line_items = []
	for variant in order_items:
		line_items.append({'variant_id':variant[0], 'quantity': variant[1]})
	post_data = {}
	# post_data['inventory_behaviour'] = "decrement_ignoring_policy"
	# post_data["fulfillment_status"]= "fulfilled"
	post_data['order'] = {'inventory_behaviour':'decrement_ignoring_policy',"fulfillment_status": "fulfilled",'line_items':line_items}
	

	shopify = ShopifyOperations()
	url = shopify.shop_url+'/orders.json'
	# print(post_data)
	header = {"Content-Type": "application/json"}
	result = requests.post(url, data=json.dumps(post_data), headers = header)
	# print(result.text)
	return render(request, "stores/product.html")

# def product_details(request):
# 	id = request.GET['id']
# 	shopify = ShopifyOperations()
# 	# new_order = shopify.create_new_order(id)
# 	product_list = shopify.inventory_info()
# 	product_info = shopify.get_product_info(id)
# 	url = shopify.shop_url+'/orders.json'
# 	print(product_info)

# 	post_data = {}
# 	post_data['order'] = {'inventory_behaviour':True, 'line_items':[{'variant_id':product_info['variant_id'], 'quantity':1}]}

   
# 	# print(post_data)
# 	header = {"Content-Type": "application/json"}
# 	result = requests.post(url, data=json.dumps(post_data), headers = header)
# 	# print(result.text)
# 	return render(request, "stores/dashboard.html", {'product_list':product_list})


def loginview(request):

	if request.method == 'POST':
		user = authenticate(username = request.POST['username'], password = request.POST['password'])
		if user is not None:
			login(request,user)
			if 'next' in request.POST:
				if request.POST['next'] is not None:
					return redirect(request.POST['next'])
			return redirect('/dashboard')	
		else:
			return render(request,'stores/login.html', {'error': 'Username and Password did not match'})
	else:
		return render(request,'stores/login.html')


def logoutview(request):
	if request.method == 'POST':
		logout(request)
		return redirect('home')					
