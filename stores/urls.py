from django.conf.urls import url
from django.contrib import admin
from . import views


urlpatterns = [

	url(r'^$', views.home , name = 'home'),
    url(r'^signup/', views.signup, name = 'signup'),
    url(r'^login/', views.loginview, name = 'login'),
    url(r'^logout/', views.logoutview, name = 'logout'),
    url(r'^dashboard/', views.dashboard, name = 'dashboard'),
    # url(r'^product/(?P<id>[0-9]+)/$', views.product_details, name = 'product_details'),
    # url(r'^product/$', views.product_details, name = 'product_details'),
    url(r'^checkout/', views.checkout, name = 'checkout'),
]
