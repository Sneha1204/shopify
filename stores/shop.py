import shopify

class ShopifyOperations:
	
	API_KEY = '064309b64db6c4ad4db5b09d8112e82c'

	PASSWORD = '02ee88d190a44bd8aadc366b09e6db40'

	Shared_Secret = '4d2ea4bbba3bd11f3e7fd85ddbac6f69'

	shop_url = "https://%s:%s@cravenbuy.myshopify.com/admin" % (API_KEY, PASSWORD)
	shopify.ShopifyResource.set_site(shop_url)
	shop = shopify.Shop.current()

# Get the current shop

	def inventory_info(self):
		product_list = []
		products = shopify.Product.find()
		for item in products:
			product = {}
			product['id'] = item.id
			product['name'] = item.title
			for variant in item.variants:
				product['price'] = variant.price
				product['stock'] = variant.inventory_quantity
				product['sku'] = variant.sku
				product['variant_id'] = variant.id
			for image in item.images:
				product['image'] = image.src
			product_list.append(product)	

		# print(product_list)	
		return (product_list)


	# Get the product info
	def get_product_info(self, id):
		
		product = shopify.Product.find(id)
		# [a for a in dir(product) if not a.startswith('__')]
		# print(dir(product))
		prod_info = {}
		prod_info['id'] = product.id
		prod_info['title'] = product.title

		for variant in product.variants:
			prod_info['price'] = variant.price
			prod_info['stock'] = variant.inventory_quantity
			prod_info['sku'] = variant.sku
			prod_info['variant_id'] = variant.id
		for image in product.images:
			prod_info['image'] = image.src	

		# print(prod_info)
		return prod_info

	def create_new_order(self,id):

		product = shopify.Product.find(id)
		for variant in product.variants:
			prod_id = variant.id

		new_order = shopify.Order()
		new_order.line_items = [{
        	"quantity": 1,
        	"variant_id": prod_id
    	}]

		x = new_order.save()
		print (x)

prod1 = ShopifyOperations()
prod1.create_new_order(2556452241472)


