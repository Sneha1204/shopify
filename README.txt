The name of the trial account created is CraveNBuy

The app has been built using Django framework.

Libraries such as ShopifyAPI, requests have been installed while building this application.

The app has the ability to signup a user and authenticate his/her login.

The app retrieves the product information from the inventory and displays it on a dashboard page.

The app supports multiple checkouts which is added in the form of rows in a table - Cart.

The app updates the stock inventory after order has been placed.

The app has been hosted on http://snehapaul.com. Please signup to implement the above functionalities.
